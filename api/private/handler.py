from api.utils.Response import Response
import sqlite3

def private_endpoint(event, context):
    connection = sqlite3.connect('test.db')
    cursor = connection.cursor()

    sql = ''' INSERT INTO item(name)
              VALUES('🥒') '''

    cursor.execute(sql)
    connection.commit()

    return Response._200(body=dict(message='Private Endpoint!'))