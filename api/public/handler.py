from api.utils.Response import Response

def public_endpoint(event, context):
    return Response._200(body=dict(message='Public Endpoint!'))