import json
import decimal
import datetime

class Response():

    RESPONSE_FORMAT = {
        "headers":  {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True
        }
    }

    @staticmethod
    def _200(body: dict) -> dict:

        format_body = json.dumps(None)

        if isinstance(body, dict):
            format_body = json.dumps(body, cls=_CustomJSONDecoder)

        Response.RESPONSE_FORMAT["statusCode"] = 200
        Response.RESPONSE_FORMAT["body"] = format_body

        return Response.RESPONSE_FORMAT

    @staticmethod
    def _401() -> dict:

        Response.RESPONSE_FORMAT["statusCode"] = 401
        Response.RESPONSE_FORMAT["message"] = "Unauthorized!"

        return Response.RESPONSE_FORMAT
    
    @staticmethod
    def _500() -> dict:

        Response.RESPONSE_FORMAT["statusCode"] = 401
        Response.RESPONSE_FORMAT["message"] = "Server Error!"

        return Response.RESPONSE_FORMAT

class _CustomJSONDecoder(json.JSONEncoder):
    def default(self, o):
        # if isinstance(o, ObjectId):
        #     return str(o)
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()
        return super(_CustomJSONDecoder, self).default(o)